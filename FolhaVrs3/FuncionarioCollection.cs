﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FolhaVrs3
{
    class FuncionarioCollection
    {
        static List<Funcionario> Funcionarios = new List<Funcionario>();

        public static void Insert(Funcionario Funcionario)
        {
            Funcionarios.Add(Funcionario);
        }

        public static Funcionario Search(Funcionario Funcionario)
        {
            foreach (Funcionario x in Funcionarios)
            {
                if (x.Cpf.Equals(Funcionario.Cpf))
                {
                    return x;
                }
            }
            return null;
        }

        public static List<Funcionario> GetFuncionarios()
        {
            return Funcionarios;
        }
    }
}
