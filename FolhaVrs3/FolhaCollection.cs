﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FolhaVrs3
{
    class FolhaCollection
    {
        static List<Folha> Folhas = new List<Folha>();

        public static void Insert(Folha Folha)
        {
            Folhas.Add(Folha);
        }

        public static Folha Search(Folha Folha)
        {
            foreach (Folha x in Folhas)
            {
                if(x.Funcionario.Cpf.Equals(Folha.Funcionario.Cpf) && x.Mes == Folha.Mes && x.Ano == Folha.Ano)
                {
                    return x;
                }
            }
            return null;
        }

        public static List<Folha> GetFolhas()
        {
            return Folhas;
        }

        public static List<Folha> GetFolhasMesAno(Folha Folha)
        {
            List<Folha> FolhasTemp = new List<Folha>();

            foreach (Folha x in Folhas)
            {
                if (x.Mes == Folha.Mes && x.Ano == Folha.Ano)
                {
                    FolhasTemp.Add(x);
                }
            }
            return FolhasTemp;
        }

    }
}
