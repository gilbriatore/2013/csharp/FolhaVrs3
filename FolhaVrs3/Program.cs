﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FolhaVrs3
{
    class Program
    {
        static void Main(string[] args)
        {
            int opc;
		    do{
                Console.WriteLine("\n\n1 - Cadastrar funcionario");
                Console.WriteLine("2 - Cadastro da folha");
                Console.WriteLine("3 - Consultar folha");
			    Console.WriteLine("4 - Listar folha");
			    Console.WriteLine("5 - Sair");
                Console.Write("Opção: ");
			    opc = int.Parse(Console.ReadLine());
			    switch(opc){
				    case 1:
					    CadastrarFuncionario();
					    break;
                    case 2:
                        CadastrarFolha();
                        break;
				    case 3:
					    ConsultarFolha();					    
					    break;
                    case 4:
                        ListarFolha();
                        break;
			    }
		    }while(opc != 5);
	    }

        private static void CadastrarFuncionario()
        {
            Console.WriteLine("\n\n");
            Funcionario Funcionario = new Funcionario();
            Console.Write("CPF: ");
            Funcionario.Cpf = Console.ReadLine();
            if (FuncionarioNegocio.ValidarCpf(Funcionario))
            {
                if (FuncionarioCollection.Search(Funcionario) == null)
                {
                    Console.Write("Nome: ");
                    Funcionario.Nome = Console.ReadLine();
                    FuncionarioCollection.Insert(Funcionario);
                }
                else
                {
                    Console.WriteLine("\n\nFuncionário já cadastrado.");
                }
            }
            else
            {
                Console.WriteLine("\n\nCPF inválido.");
            }
        }
	
	    private static void CadastrarFolha(){
		    Console.WriteLine("\n\n");
		    Folha Folha = new Folha();
            Funcionario Funcionario = new Funcionario();
            Console.Write("CPF: ");
            Funcionario.Cpf = Console.ReadLine();
            Funcionario = FuncionarioCollection.Search(Funcionario);
            if (Funcionario != null)
            {
                Folha.Funcionario = Funcionario;
                Console.Write("Mês: ");
                Folha.Mes = int.Parse(Console.ReadLine());
                Console.Write("Ano: ");
                Folha.Ano = int.Parse(Console.ReadLine());
                if (FolhaCollection.Search(Folha) == null)
                {
                    Console.Write("Horas trabalhadas: ");
                    Folha.Horas = int.Parse(Console.ReadLine());
                    Console.Write("Valor da hora: ");
                    Folha.Valor = float.Parse(Console.ReadLine());
                    FolhaCollection.Insert(Folha);
                }
                else
                {
                    Console.WriteLine("\n\nFolha já cadastrada.");
                }
            }
            else
            {
                Console.WriteLine("\n\nFuncionário não cadastrado.");
            }
            
	    }
	
	    private static void ConsultarFolha(){
            Console.WriteLine("\n\n");
            Folha Folha = new Folha();
            Funcionario Funcionario = new Funcionario();
            Console.Write("CPF: ");
            Funcionario.Cpf = Console.ReadLine();
            Folha.Funcionario = Funcionario;
            Console.Write("Mês: ");
            Folha.Mes = int.Parse(Console.ReadLine());
            Console.Write("Ano: ");
            Folha.Ano = int.Parse(Console.ReadLine());
            Folha = FolhaCollection.Search(Folha);
            if (Folha != null)
            {
                float bruto = FolhaNegocio.CalcularSalarioBruto(Folha);
                float inss = FolhaNegocio.CalcularINSS(bruto);
                float ir = FolhaNegocio.CalcularIR(bruto);
                Console.WriteLine("\n\nSalário bruto: " + bruto);
                Console.WriteLine("INSS: " + inss);
                Console.WriteLine("IR: " + ir);
                Console.WriteLine("FGTS: " + FolhaNegocio.CalcularFGTS(bruto));
                Console.WriteLine("Salário líquido: " + FolhaNegocio.CalcularSalarioLiquido(bruto, inss, ir));
            }
            else
            {
                Console.WriteLine("\n\nFolha não cadastrada.");
            }
	    }

        private static void ListarFolha()
        {
            Console.WriteLine("\n\n");
            Folha Folha = new Folha();
            Console.Write("Mês: ");
            Folha.Mes = int.Parse(Console.ReadLine());
            Console.Write("Ano: ");
            Folha.Ano = int.Parse(Console.ReadLine());
            List<Folha> Folhas = FolhaCollection.GetFolhasMesAno(Folha);
            float total = 0, ir, inss, bruto, liquido;

            foreach (Folha x in Folhas)
            {
                Console.WriteLine("Funcionário: " + x.Funcionario.Nome);
                bruto = FolhaNegocio.CalcularSalarioBruto(x);
                inss = FolhaNegocio.CalcularINSS(bruto);
                ir = FolhaNegocio.CalcularIR(bruto);
                liquido = FolhaNegocio.CalcularSalarioLiquido(bruto, ir, inss);
                Console.WriteLine("Salário líquido: " + liquido);
                total += liquido;
                Console.WriteLine("-----------------------------------------");
            }
            

            Console.Write("Total: " + total);

            Console.ReadKey();
        }
    }       
    
}
